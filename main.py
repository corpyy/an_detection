# -*- coding: utf-8 -*-
"""
Created on Fri Mar 29 14:19:44 2019

@author: Hamza
"""

import os 
import numpy as np 
import nibabel as nib

dcm_folder_path
output_folder_path

# prends cfg config file et load les dicoms avec ryu function
#cfg config contient save_path_input, save_path_blood_vessel, save_path_AN et path_input_dicom(their name)
# 
patients=os.listdir(dcm_folder_path)
    cfgInput=path_dcm+i+'.nii.gz'
    cfgNamesOfPredictions=i
    cfgMask=output+blood+i+'.nii.gz'
    
for i in patients:
    
    dp = dcmProcess(dcm_folder_path,i)
    img=dp.axial_arr
    vox=dp.voxelSize_xyz
    img=zoom(img, (vox[0]/0.4,vox[1]/0.4, vox[2]/0.4))
    mean = np.mean(img)
    std = np.std(img)
    data_nor = (img - mean) / std
    img2=nb.Nifti1Image(data_nor, np.eye(4))
    save_path = ''
    nb.save(img2,save_path_input+patient)
    
    

    # blood segmentation with deepmedic
    
    runfile('/home/hamza/deepdmedic/deepMedicRun', args='-model /mnt/data1/datasetAN/demo/deepmedic/examples/configFiles/bloodSeg/model/modelConfig.cfg -test /mnt/data1/hamza/deepmedic/examples/configFiles/bloodSeg/test/testConfig.cfg -load /mnt/data1/hamza/output/saved_models/bloodSeg/ .ckpt -dev cuda3', wdir='/mnt/data1/hamza/deepmedic/')
    
    
    #AN detection with deepmedic
    
    runfile('/home/hamza/deepdmedic/deepMedicRun', args='-model /mnt/data1/hamza/deepmedic/examples/configFiles/AN_detect/model/modelConfig.cfg -test /mnt/data1/hamza/deepmedic/examples/configFiles/AN_detect/test/testConfig.cfg -load /mnt/data1/hamza/output/saved_models/AN_detect_train/AN_detect_model.AN_detect_train.2019-03-27.13.52.52.166896.model.ckpt -dev cuda3', wdir='/mnt/data1/hamza/deepmedic/')





