#!/usr/bin/env python
# coding: utf-8

# In[11]:


import os
import numpy as np
import pydicom as dicom

class dcmProcess:
    def __init__(self,dcm_folder_path, folderName):
        if folderName.__class__ is not ''.__class__:
            raise Exception('need a string path name...')
        self.folderName = folderName
        for dirPath, dirNames, fileNames in os.walk(dcm_folder_path + self.folderName):
            pass
        self.files = [file for file in fileNames if file.endswith('.dcm')]
        self.dirPath = dirPath
        dcm = [dicom.read_file(dirPath+'/'+f) for f in self.files]
        self.axialArr, self.voxelSize, self.patientID = self.getAxialArray_XYZ(dcm)
        
    def getAxialArray_XYZ(self, dcm):
        dcm.sort(key = lambda x: float(x.ImagePositionPatient[2]))
        axial_arr = []
        axial_dcm = []
        for slices in dcm:
            if slices[0x8,0x103E].value == 'HEAD MRA TFE':
                axial_dcm.append(slices)
        axial_dcm.sort(key = lambda x: float(x.ImagePositionPatient[2]))
        axial_arr = np.stack([s.pixel_array for s in dcm if s[0x8,0x103E].value == 'HEAD MRA TFE'])
        voxelSize_xyz = np.array([axial_dcm[0].PixelSpacing[0],axial_dcm[0].PixelSpacing[1], np.abs(axial_dcm[1].SliceLocation-axial_dcm[0].SliceLocation)])
        patientID = axial_dcm[0][0x10,0x20].value
        return axial_arr, voxelSize_xyz, patientID
        
if __name__ == '__main__':
    dcm_folder_path = '/mnt/data1/Medical/cerebral_aneurysm/dcm/'
    folderName = os.listdir(dcm_folder_path)
    folderName.sort()
    for folder in folderName[1:2]:
        if not folder.startswith('.'):
            dp = dcmProcess(dcm_folder_path,folder)

